from django.urls import path
from .views import Join, sched_delete

app_name = 'myform'

urlpatterns = [
    path('', Join, name="Pelajaran"),
    path('<int:pk>', sched_delete, name="Delete")
]