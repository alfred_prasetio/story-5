# Generated by Django 3.1.1 on 2020-10-17 15:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myform', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pelajaran',
            name='SenesterTahun',
        ),
        migrations.AddField(
            model_name='pelajaran',
            name='SemesterTahun',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='pelajaran',
            name='DeskripsiMataKuliah',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='pelajaran',
            name='DosenPengajar',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='pelajaran',
            name='JumlahSKS',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='pelajaran',
            name='NamaMatkul',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AlterField(
            model_name='pelajaran',
            name='RuangKelas',
            field=models.CharField(default='', max_length=50),
        ),
    ]
