from django.db import migrations, models

class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Pelajaran',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('NamaMatkul', models.TextField(max_length = 50)),
                ('DosenPengajar', models.TextField(max_length = 50)),
                ('JumlahSKS', models.TextField(max_length=50)),
                ('DeskripsiMataKuliah', models.TextField(max_length = 100)),
                ('SenesterTahun', models.TextField(max_length = 50)),
                ('RuangKelas', models.TextField(max_length=50)),
            ],
        ),
    ]
