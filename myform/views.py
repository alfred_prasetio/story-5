from django.shortcuts import render, redirect
from .models import Pelajaran as jadwal
from .forms import PelForm

# Create your views here.

def Join(request):
    if request.method == 'POST':
        form = PelForm(request.POST)
        if form.is_valid():
            sched = jadwal()
            sched.NamaMatkul = form.cleaned_data['NamaMatkul']
            sched.DosenPengajar = form.cleaned_data['DosenPengajar']
            sched.JumlahSKS = form.cleaned_data['JumlahSKS']
            sched.DeskripsiMataKuliah = form.cleaned_data['DeskripsiMataKuliah']
            sched.SemesterTahun = form.cleaned_data['SemesterTahun']
            sched.RuangKelas = form.cleaned_data['RuangKelas']
            sched.save()
        return redirect('/Pelajaran')
    else:
        sched = jadwal.objects.all()
        form = PelForm()
        response = {'sched':sched, 'form':form}
        return render(request, 'Pelajaran.html', response)

def sched_delete(request, pk):
    if request.method == 'POST':
        form = PelForm(request.POST)
        if form.is_valid():
            sched = jadwal()
            sched.NamaMatkul = form.cleaned_data['NamaMatkul']
            sched.DosenPengajar = form.cleaned_data['DosenPengajar']
            sched.JumlahSKS = form.cleaned_data['JumlahSKS']
            sched.DeskripsiMataKuliah = form.cleaned_data['DeskripsiMataKuliah']
            sched.SemesterTahun = form.cleaned_data['SemesterTahun']
            sched.RuangKelas = form.cleaned_data['RuangKelas']
            sched.save()
        return redirect('/Pelajaran')
    else:
        jadwal.objects.filter(pk=pk),delete()
        data = jadwal.objects.all()
        form = PelForm()
        response = {'sched':data, 'form':form}
        return render(request, 'Pelajaran.html', response)