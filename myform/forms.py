from django import forms

class PelForm(forms.Form):
    NamaMatkul = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Mata Kuliah',
        'type' : 'text',
        'required' : True,
    }))

    DosenPengajar = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Dosen Pengajar',
        'type' : 'text',
        'required' : True,
    }))

    JumlahSKS = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Jumlah SKS',
        'type' : 'text',
        'required' : True,
    }))

    DeskripsiMataKuliah= forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Deskripsi Mata Kuliah',
        'type' : 'text',
        'required' : True,
    }))

    SemesterTahun = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Gasal/Genap 2019/2020',
        'type' : 'text',
        'required' : True,
    }))

    RuangKelas = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Ruang Kelas',
        'type' : 'text',
        'required' : True,
    }))