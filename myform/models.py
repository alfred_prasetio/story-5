from django.db import models

# Create your models here.
class Pelajaran(models.Model):
    NamaMatkul = models.CharField(max_length = 50, default="")
    DosenPengajar = models.CharField(max_length = 50, default="")
    JumlahSKS = models.CharField(max_length=50, default="")
    DeskripsiMataKuliah = models.CharField(max_length = 100, default="")
    SemesterTahun = models.CharField(max_length = 50, default="")
    RuangKelas = models.CharField(max_length=50, default="")
    